package com.main; 
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.Date;


public class HelloWorld extends HttpServlet {
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {
    PrintWriter out = response.getWriter();
    out.println("This message is from servlet! </p>");
    out.println("<p>Current Date/Time: " +
			new Date().toString() + "</p>");
    out.println("<p>Remote hostname: "+request.getRemoteHost()+ "</p>");
    out.println("<p>Server name : "+request.getServerName()+ "</p>");
  }
}
